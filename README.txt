Josie Crane, Brendan Murphy, Ty Swanson

Test list:
Red city at (1,1)
Blue city at (4,1)
Red archer starts at (2,0)
Red settler starts at (4,3)
Ocean at (1,0)
Hills at (0,1)
Mountain at (2,2)
Plains at (15,15)
Blue legion at (3,2)
Red goes first
Unit exists at (2,0)
No movement on mountains
No movement off board
No movement off other side of board
No movement more than one space
Red can not move blue units
No unit can move once per turn
Movement is restored at the end of a round
Production toal increments after two turns
City population is always one
Blue goes after red and red goes after blue
Round advances ages
Red wins in 3000BC
Red unit can destroy blue unit
No units on the same tile
Can not move a null unit
Can build a unit
Switch production
Build clockwise slots 1,2,3,4,5,6,7,8
Is tile able to hold unit

Iterations
Making production work after each round
We wrote a test to check that there would be an archer produced on the city. 
(Principle: Test First)
We made archer the default production unit. (Principle: Fake it ‘Till You Make It)
We made a series of tests which tested the game’s ability to make units 
(when there is enough production) in a clockwise direction, skipping tiles 
which already had units on them. (Principle: Test First) 
These methods repeated endOfTurn() more and more with each test so we 
refactored and placed units for individual tests. (Principle: Isolated Tests)
We then made a method that checked the unit to be placed (which was still just 
archer) which called another method which determined where to place the unit.
We wrote the shell of that second method that was called, but we weren’t sure 
how to implement it yet and it was late, so we stopped for the day and made plans for the next day to meet again. (Principle: Break)
Because the game ends at year 3000 and the players should be moving their 
units, we decided to keep things simple for this iteration and just make the 
game check the tile where the city is and the other tiles immediately 
around it, rather than extending out beyond the first layer of tiles 
surrounding the city.
To do this we made a for loop with if statements in it. We didn’t like the if 
statements and switched them to a switch statement, with the index as the case.
Each case had duplicated code, so we did some research and experimented to 
figure out how to move the duplicated code elsewhere and have it be just in one spot. As we brainstormed, we condensed the code more and more.
In our enthusiasm to condense the code, we took out the break statements for 
each case, and all our tests for that method failed except the one testing the final position for placing a unit.
Then we realized that without the break statements, the code still goes through 
every case, which messed us up as it would end up with only the last case taking effect, so we put break statements back in.
All tests passed.

Trying to make it so that units cannot move more than once per turn:
We wrote the test first to make sure movement capability was taken away after 
moving once. (Test First)
We had a new null pointer exception in moveUnit() which we needed to fix.
We added a variable in UnitImpl to store whether or not the unit has moved, plus 
a getter and setter for the variable. (Obvious Implementations)
Then we made an ArrayList in GameImpl to store the units that had been created 
in order to iterate through them at the end of the round and restore movement capability.(Obvious Implementations)
We wrote a test to make sure the movement was restored. (Test First)
We wrote the loop that iterated through the units and changed their moved 
variable to false again. (Obvious Implementations)
Reflections
Benefits:
There were many times where we weren’t sure how to proceed with our development, but following the TDD rhythm helped to reorient our focus and allow us to take bite sized pieces. A great example of this came from when we were trying to make production in the game. We got very overwhelmed when it came to the prospect of trying to implement every rule for producing a unit and then where it should go. When we relied on the TDD rhythm, we were able to take a small bite (writing a test to produce a single unit -> producing a single unit) and then expand from there; producing in a clockwise rotation, not producing onto non-occupiable tiles, etc.. Being able to rely on TDD and the sense of focus it brings to the project allowed for a more productive programming cycle. 
Another benefit of TDD was the ability it gave us to trust our code. If there were ever difficult parts to write, the tests would verify that our program still behaved as expected after finishing the difficult section. A benefit to that trust emerged in our ability to refactor our code. There were many times when we would write an extremely long method or our program would be difficult to read. The ability to change that section of the program and verify that it still behaved as expected allowed us to write condense, readable code. A great example of this was when we were implementing clockwise production. We wrote a method that took around 80 lines to implement. After we verified that it was correct with our test cases, we were able to change our code and see if it failed. When it failed we looked closer at our refactoring and adjusted as needed. If it passed then we were able to have confidence that our code still worked correctly. Our 80ish line method was able to be condensed to a more manageable 40. 
Negatives: 
Our group ran into a few problems with TDD. One of the main problems we had with TDD revolved around writing implementations that would just pass as soon as possible. Then as we tried to build out the software we ran into a lot of problems where we implemented a method that returned a default answer specified earlier. So, it was great to get that test to pass the first time, but as we continued to code we had to go back to the same method and implement it multiple times. 
Another problem was that it felt like we were just patching things together a lot of the time and as we tried to expand it would continuously break the program. We knew that these tests would help us in the long run, but some of the time we had to backtrack and refactor the tests. The “blue goes after red and then red goes after blue” is a great example of a test that we implemented and tested that passes now, but as we add more players this test will become obsolete. It feels sort of like a double edged sword as it feels great to have a lot of tests that pass and sure up holes in our code, but sometimes it feels like TDD encourages holes.
Lastly, it just took us some more time to start with the test rather than just implementing the method and testing later. TDD might have saved us time, but as we were coding it did not feel that way. It felt like it was taking us a lot of time to come up with a test, then implement the method to work with the test, and then refactoring and repeating this step again. This might have been more of a limitation of our group, but we all agreed that the timing that TDD requires felt like it added extra time.
