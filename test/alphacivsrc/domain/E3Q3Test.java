package alphacivsrc.domain;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class E3Q3Test {
	Transcript transcripter;

	
	@Test
	public void shouldTurnIconToOff() {
		transcripter = new Transcript(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp(), new UnitCreationAlphaCiv(), true, new ButtonViewStrategy());
		assertEquals(transcripter.getIcon(), "on.PNG");
		transcripter.getViews().get(0).transcriptButtonPress();
		assertEquals(transcripter.getIcon(), "off.PNG");
	}
	
	@Test
	public void shouldTurnIconToOn() {
		transcripter = new Transcript(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp(), new UnitCreationAlphaCiv(), false, new ButtonViewStrategy());
		assertEquals(transcripter.getIcon(), "off.PNG");
		transcripter.getViews().get(0).transcriptButtonPress();
		assertEquals(transcripter.getIcon(), "on.PNG");
	}
	
}
