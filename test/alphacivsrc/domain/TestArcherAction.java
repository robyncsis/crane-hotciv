package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestArcherAction {
	private ArcherActionImpl archerActionImpl;
	TileImpl tile;
	
	@Before
	public void setUp(){
		tile = new TileImpl(new Position(8, 8), GameConstants.PLAINS);
		archerActionImpl = new ArcherActionImpl(tile);
	}

	@Test
	public void testFortify() {
//		TileImpl tile = new TileImpl(new Position(8, 8), GameConstants.PLAINS);
		UnitImpl archer = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl(tile), tile);
		tile.setUnit(archer);
//		UnitContext unitContext = new UnitContext(tile);
		
		int originalDefense = archer.getDefensiveStrength(); 
		archerActionImpl.unitPreformsAction();
		assertEquals(originalDefense*2, archer.getDefensiveStrength());
		assertEquals(0, archer.getMoveCount());
	}
	
	@Test
	public void testUnfortify() {
		UnitImpl archer = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl(tile), tile);
		tile.setUnit(archer);
//		UnitContext unitContext = new UnitContext(tile);
		
		int originalDefense = archer.getDefensiveStrength(); 
		archerActionImpl.unitPreformsAction();
		archerActionImpl.unitPreformsAction();
		assertEquals(originalDefense, archer.getDefensiveStrength());
		assertEquals(1, archer.getMoveCount());
	}

}