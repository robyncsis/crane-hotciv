package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestGammaCiv {
	private Game game;
	private TileImpl[][] boardArray;
	
	@Before
	public void setUp(){
		game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp(), new UnitCreationGamaCiv());
		boardArray = ((GameImpl) game).getBoardArray();
	}

	@Test
	public void testArcherActionCanNotMove() {
		TileImpl tile = boardArray[9][2];
		UnitImpl unit = new UnitImpl(Player.RED,GameConstants.ARCHER, new ArcherActionImpl(tile), tile);
		tile.setUnit(unit);
		Position pos = new Position(9,2);
		game.performUnitActionAt(pos);
		assertFalse(game.moveUnit(pos, new Position(9,3)));
	}

	@Test
	public void testArcherActionStillNotMoveAfterTurn() {
		TileImpl tile = boardArray[9][2];
		UnitImpl unit = new UnitImpl(Player.RED,GameConstants.ARCHER, new ArcherActionImpl(tile), tile);
		tile.setUnit(unit);
		Position pos = new Position(9,2);
		game.performUnitActionAt(pos);
		game.endOfTurn();
		game.endOfTurn();
		assertFalse(game.moveUnit(pos, new Position(9,3)));
	}
	
	@Test
	public void testArcherActionCanMoveAfterUnfortifing() {
		TileImpl tile = boardArray[9][2];
		UnitImpl unit = new UnitImpl(Player.RED,GameConstants.ARCHER, new ArcherActionImpl(tile), tile);
		tile.setUnit(unit);
		Position pos = new Position(9,2);
		Position pos2 = new Position(9,3);
		game.moveUnit(pos, pos2);
		game.performUnitActionAt(pos2);
		game.endOfTurn();
		game.endOfTurn();
		game.performUnitActionAt(pos2);	
		assertTrue(game.moveUnit(pos2, pos));
	}
	
	@Test
	public void testSettlerPlacesCity() {
		TileImpl tile = boardArray[9][2];
		UnitImpl unit = new UnitImpl(Player.RED,GameConstants.SETTLER, new SettlerAction(tile), tile);
		tile.setUnit(unit);
		Position pos = new Position(9,2);
		game.performUnitActionAt(pos);
		CityImpl city = (CityImpl) tile.getCity();
		assertNotNull(city);
		assertEquals(Player.RED, city.getOwner());
	}
}
