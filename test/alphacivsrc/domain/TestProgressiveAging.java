package alphacivsrc.domain;
import static org.junit.Assert.*;

import org.junit.Test;

public class TestProgressiveAging {
	@Test
	public void age100YearsPerRoundBefore100BC(){
		ProgressiveAgingImpl aging = new ProgressiveAgingImpl();
		int age = -4000;
		assertEquals("Should age 100 years to -3900", -3900, aging.calculateAge(age));
	}
	
	@Test
	public void ageTo1BCFrom100BC(){
		ProgressiveAgingImpl aging = new ProgressiveAgingImpl();
		int age = -100;
		assertEquals("Should age to -1 from -100", -1, aging.calculateAge(age));
	}
	
	@Test
	public void ageTo1ADFrom1BC(){
		ProgressiveAgingImpl aging = new ProgressiveAgingImpl();
		int age = -1;
		assertEquals("Should age to 1 from -1", 1, aging.calculateAge(age));
	}
	
	@Test
	public void ageTo50ADFrom1AD(){
		ProgressiveAgingImpl aging = new ProgressiveAgingImpl();
		int age = 1;
		assertEquals("Should age to 50 from 1", 50, aging.calculateAge(age));
	}
	
	@Test
	public void age50YearsAfter50AD(){
		ProgressiveAgingImpl aging = new ProgressiveAgingImpl();
		int age = 50;
		assertEquals("Should age to 100 from 50", 100, aging.calculateAge(50));
	}
	
	@Test
	public void ageTo1775From1750(){
		ProgressiveAgingImpl aging = new ProgressiveAgingImpl();
		int age = 1750;
		assertEquals("Should age to 1775 from 1750", 1775, aging.calculateAge(age));
	}
	
	@Test
	public void ageTo1905From1900(){
		ProgressiveAgingImpl aging = new ProgressiveAgingImpl();
		int age = 1900;
		assertEquals("Should age to 1905 from 1900", 1905, aging.calculateAge(age));
	}
	
	@Test
	public void ageTo1971From1970(){
		ProgressiveAgingImpl aging = new ProgressiveAgingImpl();
		int age = 1970;
		assertEquals("Should age to 1971 from 1970", 1971, aging.calculateAge(age));
	}
	
}
