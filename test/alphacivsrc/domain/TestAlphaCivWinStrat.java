package alphacivsrc.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class TestAlphaCivWinStrat {

	private AlphaCivWinStrat winStrat;
	
	@Before
	public void setUp(){
		this.winStrat = new AlphaCivWinStrat();
	}

	@Test
	public void testNoWinnerAt4000BC() {
		assertNull(winStrat.checkForWinner(-4000, null, null));
	}
	
	@Test
	public void testRedWinsAt3000BC() {
		assertEquals(Player.RED, winStrat.checkForWinner(-3000, null, null));
	}

}
