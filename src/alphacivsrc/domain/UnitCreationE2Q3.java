package alphacivsrc.domain;

import java.util.HashMap;

public class UnitCreationE2Q3 implements UnitCreationFactory{
	
	private HashMap<String, UnitActions> actions;
	public UnitCreationE2Q3(HashMap<String, UnitActions> actions) {
		this.actions = actions;
	}

	@Override
	public UnitImpl createUnit(Player owner, String unitType, TileImpl tile) {
		UnitImpl unit;
		if(unitType.equals(GameConstants.ARCHER)) {
			if(actions.get(GameConstants.ARCHER) != null) {
				unit = new UnitImpl(owner, unitType, actions.get(GameConstants.ARCHER), tile);
			}else {
				unit = new UnitImpl(owner, unitType, new ArcherActionImpl(tile), tile);
			}
			return unit;
		}
		else if(unitType.equals(GameConstants.SETTLER)) {
			if(actions.get(GameConstants.SETTLER) != null) {
				unit = new UnitImpl(owner, unitType, actions.get(GameConstants.SETTLER), tile);
			}else {
				unit = new UnitImpl(owner, unitType, new SettlerAction(tile), tile);
			}
			return unit;
		}
		if(actions.get(GameConstants.LEGION) != null) {
			unit = new UnitImpl(owner,unitType, actions.get(GameConstants.LEGION), tile);
		}else {
			unit = new UnitImpl(owner,unitType, new NoUnitActionImpl(), tile);
		}
		return unit;
	}
	

}
