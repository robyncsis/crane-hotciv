package alphacivsrc.domain;

import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Transcript implements Game{
	ArrayList<View> views;
	ArrayList<String> log;
	Game game;
	Boolean isTranscribed;
//	static String ON = "on.PNG";
//	static String OFF = "off.PNG";
//	public String icon;
	static Icon ON = new ImageIcon("resources/on.PNG");
	static Icon OFF = new ImageIcon("resources/off.PNG");
	public Icon icon;
	
	public Transcript(Aging agingStrategy, WinStrategy winStrategy, BoardSetUp boardSetUp,
			UnitCreationFactory unitFactory, boolean isTranscribed, ViewStrategy viewStrategy) {
		log = new ArrayList<String>();
		game = new GameImpl(agingStrategy, winStrategy, boardSetUp, unitFactory);
		this.isTranscribed = isTranscribed;
		views = viewStrategy.getViews(this);

		if(isTranscribed) icon = ON;
		else icon = OFF;
	}
	
	public void printMove(Unit unit, Position from, Position to) {
		String line = unit.getOwner() + " moves " +unit.getTypeString() + " at " + from.toString() + " to " + to.toString();
		System.out.println(line);
		log.add(line);
	}

	public void printUnitAction(Unit unit, Position pos) {
		String action;
		if(unit.getTypeString().equals(GameConstants.ARCHER)){
			action = " fortify";
		}
		else if(unit.getTypeString().equals(GameConstants.SETTLER)){
			action = " settle";
		}
		else {
			action = " do something that should be impossible";
		}
		String line = unit.getOwner() + " has "+ unit.getTypeString() + " at " + pos.toString()+ action ;
		System.out.println(line);
		log.add(line);
	}
	
	public void printEndOfTurn(Player player) {
		String line = player.toString() + " ends turn";
		System.out.println(line);
		log.add(line);
	}
	
	public void printChangeProduction(City city) {
		String line = city.getOwner() + " changes production type in city at " + ((CityImpl) city).getPosition().toString() + " to " + city.getProduction();
		System.out.println(line);
		log.add(line);
	}
	
	public void printChangeWorkFocus(City city) {
		String line = city.getOwner() + " changes workforce focus in city at " + ((CityImpl) city).getPosition().toString() + " to " + city.getWorkforceFocus();
		System.out.println(line);
		log.add(line);
	}
	
	public void printWinner(Player player) {
		String line = player + " is the winner!";
		System.out.println(line);
		log.add(line);
	}
	
	public ArrayList<String> getTranscript(){
		return log;
	}

	@Override
	public Tile getTileAt(Position p) {
		return game.getTileAt(p);
	}

	@Override
	public Unit getUnitAt(Position p) {
		return game.getUnitAt(p);
	}

	@Override
	public City getCityAt(Position p) {
		return game.getCityAt(p);
	}

	@Override
	public Player getPlayerInTurn() {
		return game.getPlayerInTurn();
	}

	@Override
	public Player getWinner() {
		Player winner = game.getWinner();
		if(winner != null && isTranscribed) {
			printWinner(winner);
		}
		return winner;
	}

	@Override
	public int getAge() {
		return game.getAge();
	}

	@Override
	public boolean moveUnit(Position from, Position to) {
		if(isTranscribed) printMove(game.getUnitAt(from), from, to);
		return game.moveUnit(from, to);
	}

	@Override
	public void endOfTurn() {
		game.endOfTurn();
		if(isTranscribed) printEndOfTurn(game.getPlayerInTurn());
		
	}

	@Override
	public void changeWorkForceFocusInCityAt(Position p, String balance) {
		game.changeWorkForceFocusInCityAt(p, balance);
		if(isTranscribed) printChangeWorkFocus(game.getCityAt(p));
	}

	@Override
	public void changeProductionInCityAt(Position p, String unitType) {
		game.changeProductionInCityAt(p, unitType);
		if(isTranscribed) printChangeProduction(game.getCityAt(p));
	}

	@Override
	public void performUnitActionAt(Position p) {
		game.performUnitActionAt(p);
		if(isTranscribed) printUnitAction(game.getUnitAt(p), p);
	}
	
	public void toggleTranscription() {
		isTranscribed = !isTranscribed;
	}

	@Override
	public void setUnitActionAt(Position p, UnitActions action) {
		game.setUnitActionAt(p, action);
	}
	
	public void update() {
		for(View view: views) {
			view.update();
		}
	}
	
	public Icon getIcon() {
		return icon;
	}
	
	public Transcript getSelf() {
		return this;
	}
	
	public ArrayList<View> getViews() {
		return views;
	}
}
