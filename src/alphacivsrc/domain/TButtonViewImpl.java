package alphacivsrc.domain;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JButton;

public class TButtonViewImpl implements View {
	Controller controller;
	Transcript transcripter;
	JButton button;
	Icon icon;
	
	public TButtonViewImpl(Transcript transcripter, Controller controller) {
		this.controller = controller;	
		this.transcripter = transcripter;
		button = new JButton();
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		         controller.handleTranscriptButtonPress();
			}});
	}
	
	@Override
	public void transcriptButtonPress() {
		controller.handleTranscriptButtonPress();
	}
	
	@Override
	public void update() {
		icon = transcripter.getIcon();
		redraw();
	}
	
	@Override
	public void redraw() {
		button = new JButton(icon);
	}
	
}
