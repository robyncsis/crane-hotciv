package alphacivsrc.domain;

import java.util.ArrayList;

public class ButtonViewStrategy implements ViewStrategy {

	@Override
	public ArrayList<View> getViews(Transcript transcripter) {
		Controller controller = new ControllerImpl(transcripter);
		ArrayList<View> views = new ArrayList<View>();
		views.add(new TButtonViewImpl(transcripter, controller));
		return views;
	}

}
