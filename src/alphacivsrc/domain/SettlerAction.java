package alphacivsrc.domain;

public class SettlerAction implements UnitActions {
	
	TileImpl tile;
	
	public SettlerAction(TileImpl tile) {
		this.tile = tile;
	}

	@Override
	public void unitPreformsAction() {
//		TileImpl tile = unitContext.getTile();
		UnitImpl unit = tile.getUnit();
		tile.setCity(new CityImpl(unit.getOwner(), tile.getPosition()));
	}

}
