package alphacivsrc.domain;

import java.util.ArrayList;

public interface ViewStrategy {
	
	 public ArrayList<View> getViews(Transcript transcripter);

}
