package alphacivsrc.domain;

public class UnitCreationAlphaCiv implements UnitCreationFactory {

	@Override
	public UnitImpl createUnit(Player owner, String unitType, TileImpl tile) {
		UnitImpl unit = new UnitImpl(owner,unitType, new NoUnitActionImpl(), tile);
		return unit;
	}

}
