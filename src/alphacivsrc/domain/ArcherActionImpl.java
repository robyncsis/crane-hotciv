package alphacivsrc.domain;

public class ArcherActionImpl implements UnitActions {
	TileImpl tile;
	
	public ArcherActionImpl(TileImpl tile) {
		this.tile = tile;
	}

	@Override
	public void unitPreformsAction() {
		UnitImpl unit = tile.getUnit();
		if(unit.getMoveCount() == 0) {
			unit.setDefensiveStrength(unit.getDefensiveStrength()/2);
			unit.setMoveCount(1);
		}else {
			unit.setDefensiveStrength(unit.getDefensiveStrength()*2);
			unit.setMoveCount(0);
		}
	}
}
