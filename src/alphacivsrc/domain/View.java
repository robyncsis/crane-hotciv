package alphacivsrc.domain;

public interface View {
	
	public void transcriptButtonPress();
	
	public void update();
	
	public void redraw();
}
