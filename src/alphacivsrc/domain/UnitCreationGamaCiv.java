package alphacivsrc.domain;

public class UnitCreationGamaCiv implements UnitCreationFactory {

	@Override
	public UnitImpl createUnit(Player owner, String unitType, TileImpl tile) {
		if(unitType.equals(GameConstants.ARCHER)) {
			UnitImpl unit = new UnitImpl(owner, unitType, new ArcherActionImpl(tile), tile);
			return unit;
		}else if(unitType.equals(GameConstants.SETTLER)) {
			UnitImpl unit = new UnitImpl(owner, unitType, new SettlerAction(tile), tile);
			return unit;
		}
		UnitImpl unit = new UnitImpl(owner,unitType, new NoUnitActionImpl(), tile);
		return unit;
	}

}
