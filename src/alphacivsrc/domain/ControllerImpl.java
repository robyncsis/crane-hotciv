package alphacivsrc.domain;

public class ControllerImpl implements Controller {
	Transcript transcripter;
	public ControllerImpl(Transcript transcripter) {
		this.transcripter = transcripter;
	}
	
	public void handleTranscriptButtonPress() {
		transcripter.toggleTranscription();
	}
}
