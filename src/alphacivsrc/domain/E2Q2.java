package alphacivsrc.domain;
import java.util.ArrayList;

import thirdparty.ThirdPartyFractalGenerator;

public class E2Q2 extends DeltaCivSetUp {
	
	ThirdPartyFractalGenerator generator;
	TileImpl[][] boardArray;
	DeltaCivSetUp dcsu;
	
	public E2Q2(TileImpl[][] boardArray) {
		super(boardArray);
		//dcsu = new DeltaCivSetUp(boardArray);
	}
	
	public E2Q2() {
		super();
		//dcsu = new DeltaCivSetUp();
	}
	
	public E2Q2(ThirdPartyFractalGenerator generator) {
		super();
		this.generator = generator;
		//dcsu = new DeltaCivSetUp(boardArray);
	}

	@Override
	public TileImpl[][] setUpBoard(UnitCreationFactory unitFactory){
		TileImpl[][] board = new TileImpl[GameConstants.WORLDSIZE][GameConstants.WORLDSIZE];
		for(int i = 0; i < 16; i++) {
			for (int j = 0; j < 16; j++) {
				char c = generator.getLandscapeAt(i, j);
				switch (c) {
					case '.':
						board[i][j] = new TileImpl(new Position(i, j), GameConstants.OCEANS);
						break;
					case 'o':
						board[i][j] = new TileImpl(new Position(i, j), GameConstants.PLAINS);
						break;
					case 'f':
						board[i][j] = new TileImpl(new Position(i, j), GameConstants.FOREST);
						break;
					case 'h':
						board[i][j] = new TileImpl(new Position(i, j), GameConstants.HILLS);
						break;
					case 'M':
						board[i][j] = new TileImpl(new Position(i, j), GameConstants.MOUNTAINS);
						break;
				}
			}
		}
		this.boardArray = board;
		return board;
	}
	
//	@Override
//	public ArrayList<CityImpl> getCities() {
//		return dcsu.getCities();
//	}
//
//	@Override
//	public ArrayList<UnitImpl> getUnits() {
//		return dcsu.getUnits();
//	}
	
	
	
}
