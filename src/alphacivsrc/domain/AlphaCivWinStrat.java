package alphacivsrc.domain;

import java.util.ArrayList;

public class AlphaCivWinStrat implements WinStrategy {

	@Override
	public Player checkForWinner(int age, ArrayList<CityImpl> cities, Player playerInTurn) {
		if(age >= -3000) {
			return Player.RED;
		}
		return null;
	}

}
