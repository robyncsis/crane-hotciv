package alphacivsrc.domain;

public class ProgressiveAgingImpl implements Aging{

	@Override
	public int calculateAge(int year) {
		if(year < -100) {
			return year+100;
		}
		else if(year == -100) {
			return -1;
		}
		else if(year == -1) {
			return 1;
		}
		else if(year == 1){
			return 50;
		}
		else if(year >= 50 && year < 1750) {
			return year+50;
		}
		else if(year >= 1750 && year < 1900) {
			return year+25;
		}
		else if(year >= 1900 && year < 1970) {
			return year+5;
		}
		return year+1; //year >= 1970
	}
	
}
